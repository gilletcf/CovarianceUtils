# Validation of the CovarianceVectorMultiplySolver

The spatial correlation function at a given node $z_i$ corresponds to the $i$-th column of the covariance matrix $C$. It can be visualized by plotting the result of applying $C$ to a vector that has a value of one at $z_i$ and a value of zero at all other points (Guillet et al., 2019).  

Case\_1.sif and Case\_2.sif implement different methods and parameters for the correlation functions.
These test cases are derived from the ElmerIce unitary tests:     
- [ELMER_TRUNK]/elmerice/Tests/CovarianceVector   
- [ELMER_TRUNK]/elmerice/Tests/CovarianceVector2   

The numerical solutions are compared to their analytical counterparts in [Validation.ipynb](Validation.ipynb)

## Running the test cases:

1. Create the mesh:   
> ./MakeMesh.sh

2. Compile/Get required codes to run the test:   
> make

3. Run tests:   
> ElmerSolver Case_1.sif   
> ElmerSolver Case_2.sif
