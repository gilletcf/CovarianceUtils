# Elmer/Ice test cases for CovarianceUtils

Test cases and set-up for Elmer/Ice solvers requiring operations involving a covariance matrix $B$.

See the associated Elmer/Ice [Documentation](https://github.com/ElmerCSC/elmerfem/tree/elmerice/elmerice/Solvers/Documentation/CovarianceUtilsModule.md)

The implementation mainly follows Guillet *et al.* (2019) where the operations requiring $B$ are replaced by an equivalent operator that is easily implemented with the FEM for unstructured meshes. 

Guillet O., Weaver A.T., Vasseur X., Michel Y., Gratton S., Gurol S. Modelling spatially correlated observation errors in variational data assimilation using a diffusion operator on an unstructured mesh. Q. J. R. Meteorol. Soc., 2019. https://doi.org/10.1002/qj.3537

## Content:

1. Application test cases:   
   - [MassConservation](MassConservation): Sensitivity of the Mass Conservation Method to the Regularisation scheme. Introducing a prior and prior error statitics require the inverse of the covariance matrix. This done using the **BackgroundErrorCostSolver**.   
   - [FrictionTestCase](FrictionTestCase): Twin experiement for the inversion of the basal friction coefficient using the **BackgrounErrorCostSolver**.   
   - [GaussianSimulationTestCase](GaussianSimulationTestCase): Test case to generate random realisation from the prescribed coavariance matrix $B$. This requires a square-root of $B$ and this is done using the **GaussianSimulationSolver**.   
   - [FilterTestCase](FilterTestCase) : Test case to filter noisy data directly on the mesh. This uses the **CovarianceVectorMultiplySolver**


2. Validation test cases:   
   - [CovarianceTestCase](CovarianceTestCase) : Validate the correlation functions; i.e. compute the product $B.x$ where $x$ is an impulse (=1 in one node, =0 elsewhere).




