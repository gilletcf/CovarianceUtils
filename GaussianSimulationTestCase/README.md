# Test the GaussianSimulationSolver

Compute random draws for the prescribed covariances matrices using the *GaussianSimulationSolver*.

Test for different methods and parameters for the covariance matrix.

Results visualisation: [GaussianSimulations.ipynb](GaussianSimulations.ipynb)

## Running the test cases:

1. Create the mesh:   
> ./MakeMesh.sh

3. Run tests:   
> ElmerSolver [File].sif
