# Test case

Directory to run an inversion test case.

1. Generate the mesh in the [Mesh](../Mesh) directory.   
2. Generate a perfect model solution in the [Init](../Init) directory.   
3. Edit the std.nco file that defines the standard deviation to generate the random noise.   
4. Run the script ../scripts/RunTest.sh 
