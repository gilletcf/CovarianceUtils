# Scripts used to run a test case



## Content 

- InitObs.sh: copy results from the perfect model observations ([init](../Init)) and generate noisy observations. Uses:   
   - random.nc2: a ncap2 script used to generate random noise on the observations grid.   
   - std.nco: a parameter file for random.nc2 that defines the standard deviation to generate the noise from a normal distribution.   

- MakeL_Curve.sh: run a L_Curve optimisation for the Tikhonov regularisation; i.e. test several values of the regularisation weight.   

- MakeSensitivity.sh: run a sensitivity test for the covariance regularisation; i.e. test several values of the range and standar deviation.   

- RunTest.sh: Call the script above in sequence.   

