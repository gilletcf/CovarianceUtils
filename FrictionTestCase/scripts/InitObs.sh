#!/bin/bash
####################################################################
# Initialisation of the velocity observations
#	
# Copy the mesh and results from the Init dir. and process the 
# output gridded results to be used as observations
#
# A random hite noise defined in the parameter file random.nc2
# is added to the observations
#
# OPTIONAL INPUT PARAMETERS:
#  stride: to sample the obsersvations (default: 2)
####################################################################

cp -r ../Init/rectangle .

if [ "$#" -eq 1 ]
then
  sample=$1
else
  sample=2
fi

echo $sample

export GSL_RNG_TYPE=ranlux
export GSL_RNG_SEED=129383

ncwa -a Time rectangle/Grid_ssa.nc tmp.nc
ncap2 -O -S ../scripts/random.nc2 tmp.nc tmp2.nc
ncks -O -F -dx,2,-2,$sample -dy,2,-2,$sample tmp2.nc  observations.nc

rm -f tmp.nc tmp2.nc



