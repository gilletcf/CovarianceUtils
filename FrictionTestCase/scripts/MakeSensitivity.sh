#!/bin/bash
######################################################@
# Sensitivity experiment to the covariance parameters
#
######################################################@
# Parameters
range='1000.0 2000.0 4000.0'
std='1.41421e-3 2.0e-3 2.8284e-3'
nu=1


# loop over reg. coefs.
c=0
for r in $range
do
  for s in $std
  do	  
    c=$((c+1))

    echo $i
    # get .sif file
    NAME="$c"
    sed  "s/<range>/"$r"/g;s/<std>/"$s"/g;s/<nu>/"$nu"/g;s/<ID>/$c/g" ../sif/OPTIM_TWIND_ssa_COV.tp.sif > OPTIM_TWIND_ssa_COV"$c".sif

  # run 
    ElmerSolver OPTIM_TWIND_ssa_COV"$c".sif

  done
done

