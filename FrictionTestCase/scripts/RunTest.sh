#!/bin/bash
#######################################
# run a test case
#
# - to be run from a test directory
######################################
if [ "$#" -eq 1 ]
then
  step=$1
else
  step=2
fi
# copy intial results and generate observations
../scripts/InitObs.sh $step
# Test with backgroundError
../scripts/MakeSensitivity.sh
# Test with frt derivative penalisation
../scripts/MakeL_Curve.sh 
