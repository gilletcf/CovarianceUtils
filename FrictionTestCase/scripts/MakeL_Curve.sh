#!/bin/bash
######################################################@
# Sensitivity experiment to the regularisation weight
#
######################################################@

# Parameters
lambda='1.0e4 5.0e4 1.0e5 5.0e5'

rm -rf LCurve.dat

# loop over reg. coefs.
c=0
for i in $lambda
do
  c=$((c+1))

  echo $i
  # get .sif file
  NAME="$c"
  sed  "s/<Lambda>/"$i"/g;s/<ID>/$c/g" ../sif/OPTIM_TWIND_ssa_T.tp.sif > OPTIM_TWIND_ssa_T"$c".sif

  # run 
    ElmerSolver OPTIM_TWIND_ssa_T"$c".sif

  # post process
  echo $(tail -n 1 Cost_TWIND_nl_T"$c".dat | awk '{print $4}') $(tail -n 1 CostReg_TWIND_nl_T"$c".dat | awk '{print $2}') $i >> LCurve.dat
done

