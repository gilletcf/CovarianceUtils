# Elmer/Ice configuration files

Template files to run an inversion of the basal friction coefficient using either the classical Tikhonov regularisation (OPTIM_TWIN_ssa_T.tp.sif) or using the BackgroundErrorCostSolver (OPTIM_TWIN_ssa_COV.tp.sif)

See the scripts under [scripts](../scripts) to run a test case and use these template files.


