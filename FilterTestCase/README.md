# Filtering noisy data

We use the *CovarianceVectorMultiplySolver* to filter a noisy nodal field on the mesh.

Applying $y = C.x / C.1$ has the same effect as applying a filter with the covariance function used to contruct the covariance matric $C$

[GaussianSmoother.R](GaussianSmoother.R) is used to produce 2 rasters with the same resolution as the Elmer mesh squareT:
 - raster.nc: raster with a random variable sample from a unifor distribution between 0 and 1
 - raster_rg.nc: the previous raster smoothed with a Gaussian Filter ($\sigma=0.04$)

[Filter.sif](Filter.sif): ElmerIce set-up to do the same thing direclty on the mesh:   
- import the raster on a mesh with the same horizontal resolution
- compute $y = C.x / C.1$ using the mathern diffusion operatior with a range $r=\sigma/\sqrt{2m-4}$ and large $m$

## Results

The following figures compare the 2 methods (Gaussian Filtering with R and Filter with the Elmer/Ice solver CovarianceVectorMultiplySolver with the diffusion operator method).

As expected results are similar except near the boundaries where the diffusion operator method is less accurate.

- Plane view:   
![result1](Figures/Result.png)

- Cross section:   
![result2](Figures/Result2.png)

