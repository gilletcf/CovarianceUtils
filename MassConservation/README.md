# The Mass Conservation Method

## Description

Elmer/Ice example for the mass conservation method.

Test the sensitivity to several regularisation schemes as explained in the following presentation: [MassConservation.pdf](MassConservation.pdf)

This folver is the set-up for experiment Exp1-Bed1.

Just note that as ramdom noise is added to the perfect model solution, different results could be obtained from the results prensented.


## Content

- [src](src): Elemr/Ice sourec code to run this test case.

- [rectangle2](rectangle2): Elmer/Ice mesh used to generate the perfect model solution   
- [result.nc](result.nc): UGRID file with the perfect model solution defined on rectangle2

- [zoom](zoom): Elmer/Ice mesh for the test case

- [HSteady_0.sif](HSteady_0.sif): Recompute Ice thickness with no noise; should give results very close to the perfect model solution
- [HSteady_0.sif](HSteady_n.sif): Same but with noise added  

- [PARAMETERS.sif](PARAMETERS.sif): Generic parameters for the optimisation set-up  

- [Optim_m2_1.sif](Optim_m2_1.sif): The "optimal" Matern optimisation; i.e. with the Matern function fitted from the true bed  
- [Optim_m2_2.sif](Optim_m2_2.sif): The "Mean" Matern optimisation; i.e. Matern parameters are averaged values   

- [Optim_exp_2.sif](Optim_exp_2.sif): Optimisation with the exponential correlation function   

- [Optim_Thik1.5.sif](Optim_Thik1.5.sif): The "Optimal" Tikhonov Regularisation   

## Running the test case

1. Compile and get required codes.

```
# The BackgroundErrorSolver is used twice; so get a copy of the ElmerIceSolvers lib
cp ${ELMER_HOME}/share/elmersolver/lib/ElmerIceSolvers* ElmerIceSolvers2
# Compile user-function to generate random perturbations
elmerf90 src/Random.F90 -o Random
```

2. Initialisation: Recompute steady-state ice thickness from perfect model solution

```
# no noise added; solution should be close to the perfect model solution that has been obtained from a Full-Stokes solution
# relaxed under constant forcing
ElmerSolver HSteady_0.sif

# same but with perturbed velocity and thickness
ElmerSolver HSteady_n.sif
```
3. Optimisation test cases:
```
## "Optimal3 Matern solution
ElmerSolver Optim_m2_1.sif

## "Mean" Matern solution 
ElmerSolver Optim_m2_2.sif

## Exponential correlation function
ElmerSolver Optim_exp_2.sif

## Tikhonov Regularisation
ElmerSolver Optim_Thik1.5.sif
```


