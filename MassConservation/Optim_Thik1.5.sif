!-----------------------------
! PARAMETERS:
!----------------------------
$ID=5
!# regularisation parameter
$LambdaReg=5.0e-5
!##
$name="Optim_Thk1_"
!number of iterations and output intervals
$niter=2000
$nout=100

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
include PARAMETERS.sif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Header
  Mesh DB "." "zoom"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Simulation
  Coordinate System  = Cartesian 2D 
  projection type = String "regular"

  Simulation Type = Steady

  Steady State Min Iterations = 1
  Steady State Max Iterations = $niter

  Post File = "$name$_$ID$.vtu"
  Output intervals = $nout

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Restart from HSteady_n
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Restart File = "HSteady_n.result"
  Restart Position =  0
  Restart Time = Real 0.0

  Restart Before initial conditions = Logical True

  max output level = 3
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! The ice 
Body 1
  Equation = 1
  Body Force = 1
  Initial Condition = 1
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Initial Condition 1

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! The prior for the velocity
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Vp 1  = Equals Velocity 1
  Vp 2 =  Equals Velocity 2

   Mask = Real 1.0
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body Force 1
  Top Surface Accumulation = Real 0.0
  Bottom Surface Accumulation = Real 0.0

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Bed is zs_obs-H
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  bed = Variable Zs_obs,HSteady
    REAL LUA "tx[0]-tx[1]"

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Compare to perfect model solution
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  db = Variable bed, Zb_obs
    REAL LUA "tx[0]-tx[1]"

  dh  = Variable H_obs,Hsteady
    REAL LUA "tx[0]-tx[1]"

  du 1 = Variable Velocity 1, V_obs 1
    REAL LUA "tx[0]-tx[1]"
  du 2 = Variable Velocity 2, V_obs 2
    REAL LUA "tx[0]-tx[1]"

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! dJ/dh = dJ/db * db/dh(=-1)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Hsteadyb = Variable bedb
    REAL LUA "-tx[0]"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Compute Ice thickness 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solver 1
   Equation = "Thickness"
   Variable = "HSteady"

  Procedure = "ElmerIceSolvers" "AdjointThickness_ThicknessSolver"

   Linear System Solver = Direct
   Linear System Direct Method = umfpack

 Flow Solution Name = String "Velocity"

 Exported Variable 1 = -dofs 2 "Vp"

 Exported Variable 2 = -global CostValue
 Exported Variable 3 = Mask

 Steady State Convergence Tolerance = 1.0e-16
End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Some stats on the error
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solver 2
  Equation = "Compare"
  Variable = -nooutput upd
  Procedure = "ElmerIceSolvers" "UpdateExport"

  Exported Variable 1 = bed
  Exported Variable 2 = bedb

  Exported Variable 3 = db
  Exported Variable 4 = -dofs 2 "du"
  Exported Variable 5 = dh
End

Solver 3
 Equation = "SaveScalars"

 Procedure = "SaveData" "SaveScalars"

 Filename = "Scalars_$name$_$ID$.dat"

 Variable 1 = Time

 Variable 2 = db
 Operator 2 = rms
 Operator 3 = mean

 Variable 4 = du 1
 Operator 4 = rms
 Operator 5 = mean

 Variable 6 = du 2
 Operator 6 = rms
 Operator 7 = mean

 Variable 8 = Hsteady
 Operator 8 = int

 Variable 9 = H_Obs
 Operator 9 = int

 Variable 10 = dh
 Operator 10 = rms
 Operator 11 = mean
End


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Compute Cost
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solver 4
  Equation = "Cost"
    procedure = "ElmerIceSolvers" "Adjoint_CostDiscSolver"

   Cost Variable Name = String "CostValue"  ! Name of Cost Variable
   Lambda = Real $1.0/sigma_b^2
 ! save the cost as a function of iterations (iterations,Cost,rms=sqrt(2*Cost/Ndata)
   Cost Filename = File "Cost_$name$_$ID$.dat"

   Netcdf Var Name = File "zbp"

   Observed Variable Name = String "bed"
   Observed Variable dimension = Integer 1

   Observation File Name = File "$OBSERVATION_FILE$"
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Compute regularisation
!  has to be before the adjoint as in depends on H
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solver 5
  Equation = String "CostReg"
  procedure = "ElmerIceSolvers" "Adjoint_CostRegSolver"

  !# True if cost function and gradient must be initialised to 0 in this solve
  Reset Cost Value = Logical False

  Cost Filename = File "CostReg_$name$_$ID$.dat"

  Lambda=Real $LambdaReg

  Cost Variable Name = String "CostValue"
  Gradient Variable Name = String "bedb"
  Optimized Variable Name = String "bed"

  A priori Regularisation = Logical False
end


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! dJ/dh=DJ/dbed * dbed / dh
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solver 6
  Equation = "Derivative"
  Variable = -nooutput upd
  Procedure = "ElmerIceSolvers" "UpdateExport"

  Exported Variable 1 = HSteadyb
End

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  adjoint
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solver 7
  Equation = "Adjoint"
  Variable = -nooutput Adjoint
  Variable Dofs = 1

  procedure = "ElmerIceSolvers" "Adjoint_LinearSolver"

!Name of the flow solution solver
  Direct Solver Equation Name = string "Thickness"

   Linear System Solver = Direct
   Linear System Direct Method = umfpack
End


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  Gradient
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solver 8
  Equation = "DJDp"

  procedure = "ElmerIceSolvers" "AdjointThickness_GradientSolver"

  Thickness Solution Name = String HSteady
  Adjoint Solution Name = String "Adjoint"
  Flow Solution Name = String "Velocity"

  ComputeDJDUV = Logical True
  ComputeDJDsmbTop = Logical True

  Exported Variable 1 = DJDUV
  Exported Variable 1 DOFs = 2

  Exported Variable 2 = DJDsmbTop
  Exported Variable 2 DOFs = 1
end

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  Background term for velocity
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solver 9
  Equation = "RegU"
  procedure = "ElmerIceSolvers2" "BackgroundErrorCostSolver"
  Variable = -nooutput "dumy"

  Cost Filename = File "Reg_U_$name$_$ID$.dat"
  Variable Name = String "Velocity"
  Background Variable Name = String "Vp"
  Gradient Variable Name = String "DJDUV"
  Cost Variable Name= String "CostValue"

  !# True if cost function and gradient must be initialised to 0 in this solve
  Reset Cost Value = Logical False

  Covariance type = String "diagonal"
  standard deviation = Real $sigma_v
end
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  Optimisation
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 10
  Equation = "Optimize_m1qn3"
  procedure = "ElmerIceSolvers" "Optimize_m1qn3Parallel"

  Optimisation Mask Variable = String "Mask"

  Cost Variable Name = String "CostValue"
  Optimized Variable Name = String "Velocity"
  Gradient Variable Name = String "DJDUV"
  gradient Norm File = String "GradientNormAdjoint_$name$_$ID$.dat"

  Mesh Independent = Logical False

! M1QN3 Parameters
  M1QN3 dxmin = Real 1.0e-10
  M1QN3 epsg = Real  1.e-08
  M1QN3 niter = Integer $niter
  M1QN3 nsim = Integer $niter
  M1QN3 impres = Integer 5
  M1QN3 DIS Mode = Logical True
  M1QN3 df1 = Real 0.5
  M1QN3 normtype = String "dfn"
  M1QN3 OutputFile = File  "M1QN3_$name$_$ID$.dat"
  M1QN3 ndz = Integer 10
end


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Equation 1
  Active Solvers(10) = 1 2 3 4 5 6 7 8 9 10
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Boundary Condition 1
  Name = "bottom"
  Target Boundaries = 1

  Hsteady = Variable H_Obs,Hp
    REAL LUA "tx[0]+tx[1]"
End

Boundary Condition 2
  Name = "right"
  Target Boundaries = 2

End

Boundary Condition 3
  Name = "top"
  Target Boundaries = 3

  Hsteady = Variable H_Obs,Hp
    REAL LUA "tx[0]+tx[1]"
End

Boundary Condition 4
  Name = "left"
  Target Boundaries = 4

  Hsteady = Variable H_Obs,Hp
    REAL LUA "tx[0]+tx[1]"

  Mask = Real -1.0
End

